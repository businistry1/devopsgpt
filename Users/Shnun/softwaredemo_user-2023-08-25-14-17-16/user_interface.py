# User Interface

# Function to display the registration form
def display_registration_form():
    # Implement the logic to display the registration form HTML
    registration_form_html = """
    <form>
        <!-- Registration form fields -->
    </form>
    """
    return registration_form_html

# Function to display the login form
def display_login_form():
    # Implement the logic to display the login form HTML
    login_form_html = """
    <form>
        <!-- Login form fields -->
    </form>
    """
    return login_form_html

# Function to display the create to-do item form
def display_create_todo_item_form():
    # Implement the logic to display the create to-do item form HTML
    create_todo_item_form_html = """
    <form>
        <!-- Create to-do item form fields -->
    </form>
    """
    return create_todo_item_form_html

# Function to display the edit to-do item form
def display_edit_todo_item_form(todo_item):
    # Implement the logic to display the edit to-do item form HTML with pre-filled fields
    edit_todo_item_form_html = """
    <form>
        <!-- Edit to-do item form fields with pre-filled values -->
    </form>
    """
    return edit_todo_item_form_html

# Function to display the to-do item details
def display_todo_item_details(todo_item):
    # Implement the logic to display the to-do item details HTML
    todo_item_details_html = """
    <div>
        <!-- To-do item details -->
    </div>
    """
    return todo_item_details_html

# Function to display all to-do items
def display_all_todo_items(todo_items):
    # Implement the logic to display all to-do items HTML
    all_todo_items_html = """
    <div>
        <!-- All to-do items -->
    </div>
    """
    return all_todo_items_html

# Function to display error messages
def display_error_message(message):
    # Implement the logic to display the error message HTML
    error_message_html = """
    <div>
        <!-- Error message -->
    </div>
    """
    return error_message_html
