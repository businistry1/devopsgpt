#!/bin/bash

# Start the backend server
sh run.sh start_backend python3 &

# A brief pause to ensure the backend starts without issues
sleep 2

# Start the frontend server
sh run.sh start_frontend python3 &

# Optional: Print messages to notify the user
echo "Backend and Frontend servers have been started."
